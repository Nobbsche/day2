#[cfg(test)]
mod tests {
    use std::fs;

    fn op_code_1(mut complete_program: Vec<u32>, source_a: usize, source_b: usize, target: usize ) -> Vec<u32> {
        let result_adress = complete_program[target].clone() as usize; 
        complete_program[result_adress] = complete_program[complete_program[source_a] as usize] 
                                    + complete_program[complete_program[source_b] as usize ];
        complete_program
    }

    fn op_code_2(mut complete_program: Vec<u32>, source_a: usize, source_b: usize, target: usize ) -> Vec<u32> {
        let result_adress = complete_program[target].clone() as usize; 
        complete_program[result_adress] = complete_program[complete_program[source_a] as usize] 
                                    * complete_program[complete_program[source_b] as usize ];
        complete_program
    }

    #[test]
    fn op_code_1_test() {
        let mut program = vec![1,9,10,3,2,3,11,0,99,30,40,50];
        program = op_code_1(program, 1, 2, 3);
        assert_eq!(program[3], 70);
    }

    #[test]
    fn op_code_2_test() {
        let mut program = vec![1,9,10,3,2,3,11,0,99,30,40,50];
        program = op_code_1(program, 1, 2, 3);
        program = op_code_2(program, 5, 6, 7);
        assert_eq!(program[0], 3500);
    }

    fn run_program(mut program: Vec<u32>) -> u32 {
        for operation in (0..program.len()).step_by(4) {
            match program[operation] {
                1 => program = op_code_1(program, operation + 1, operation + 2, operation + 3),
                2 => program = op_code_2(program, operation + 1, operation + 2, operation + 3),
                99 => break,
                _ => break
            }
        }

        program[0]
    }

    fn run_input_program(program: Vec<u32>, expected_result: u32 ) -> u32 {
        for noun in 0..100  {
            for verb in 0..100 {
                let mut mod_program = program.clone();
                mod_program[1] = noun;
                mod_program[2] = verb;
                if run_program(mod_program) == expected_result {
                    println!("noun: {} verb: {}", noun, verb);
                    return noun*100 + verb;
                }
            }

        }
        0
    }


    fn read_contents_from_file(filename: String) -> Vec<u32> {
        let contents = fs::read_to_string(filename)
            .expect("Something went wrong reading the file");
        contents.split(",").map(|x| { x.parse().unwrap()} ).collect()
    }

    #[test]
    fn run_program_test() {
        let mut program = vec![1,9,10,3,2,3,11,0,99,30,40,50];
        assert_eq!(run_program(program),3500);

        program = vec![1,0,0,0,99];
        assert_eq!(run_program(program),2);

        program = vec![2,3,0,3,99];
        assert_eq!(run_program(program),2);

        program = vec![2,4,4,5,99,0];
        assert_eq!(run_program(program),2);

        program = vec![1,1,1,4,99,5,6,0,99];
        assert_eq!(run_program(program),30)
    }

    #[test]
    fn read_file_test() {
        assert!( read_contents_from_file(String::from("src/program_day2_1.txt")).len() > 0 );
    }

    #[test]
    fn run_puzzle_program() {
        let mut program : Vec<u32> = read_contents_from_file(String::from("src/program_day2_1.txt"));

        program[1] = 12;
        program[2] = 2;

        assert_eq!(run_program(program), 2890696);
    }

    #[test]
    fn find_right_word() {
        let program : Vec<u32> = read_contents_from_file(String::from("src/program_day2_1.txt"));
        assert_eq!(run_input_program(program.clone(), 2890696), 1202);
        assert_eq!(run_input_program(program.clone(), 19690720), 8226);
    }
}
